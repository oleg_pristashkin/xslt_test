<?php

# GET XML FROM URL
$sXml = file_get_contents("https://www.w3schools.com/xml/plant_catalog.xml");

# LOAD XML FILE
$XML = new DOMDocument();
$XML->loadXML( $sXml );

# START XSLT
$xslt = new XSLTProcessor();
$XSL = new DOMDocument();
$XSL->load( 'templates/other.xsl', LIBXML_NOCDATA);

$xslt->importStylesheet( $XSL );

#PRINT
print $xslt->transformToXML( $XML );
?>
