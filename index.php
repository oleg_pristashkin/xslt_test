<?php

# GET XML FROM URL
$sXml = file_get_contents("https://www.w3schools.com/xml/plant_catalog.xml");

# LOAD XML FILE
$XML = new DOMDocument();
$XML->loadXML( $sXml );

# START XSLT
$xslt = new XSLTProcessor();
$XSL = new DOMDocument();
$XSL->load( 'templates/index.xsl', LIBXML_NOCDATA);

$xslt->importStylesheet( $XSL );

# SET PLANTS CATEGORY FILTER
# SET category PARAMETER FOR FILTERING
$category = $_GET['param'] ?? '';
$xslt->setParameter('', 'category', $category);

#PRINT
print $xslt->transformToXML( $XML );
?>
