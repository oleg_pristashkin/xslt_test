<?php

# GET XML FROM URL
$sXml = file_get_contents("program.xml");


# LOAD XML FILE
$XML = new DOMDocument();
$XML->loadXML( $sXml );

# START XSLT
$xslt = new XSLTProcessor();
$XSL = new DOMDocument();
$XSL->load( 'schedule.xsl', LIBXML_NOCDATA);

$xslt->importStylesheet( $XSL );

#PRINT
print $xslt->transformToXML( $XML );
?>
