<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:key name="product" match="/CATALOG/PLANT/BOTANICAL/text()" use="." />
    <xsl:template match="/">
        <html>
            <body>
                <link rel="stylesheet" type="text/css" href="/public/css/main.css" />

                <div class="sidenav">
                    <a href="/">All Categories</a>
                    <xsl:for-each select="/CATALOG/PLANT/BOTANICAL/text()[generate-id()
                                       = generate-id(key('product',.)[1])]">
                        <xsl:sort select="."/>
                        <a href="?param={.}"><xsl:value-of select="."/></a>
                    </xsl:for-each>
                </div>

                <div class="main">
                    <h2>Plants
                        <xsl:if test="not($category='')">
                            , <xsl:value-of select="$category"/> category
                        </xsl:if>
                    </h2>
                    <table border="0" style="width: 80%">
                        <tr bgcolor="#563d7c" style="color:white">
                            <th>Plant Name</th>
                            <th>Price</th>
                        </tr>
                        <xsl:for-each select="CATALOG/PLANT">
                            <xsl:if test="BOTANICAL=$category or $category=''">
                                <xsl:choose>
                                    <xsl:when test="(position() mod 2) != 1">
                                        <tr bgcolor="#e9ecef">
                                            <td>
                                                <xsl:value-of select="COMMON"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="PRICE"/>
                                            </td>
                                        </tr>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <tr>
                                            <td>
                                                <xsl:value-of select="COMMON"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="PRICE"/>
                                            </td>
                                        </tr>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:for-each>
                    </table>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
